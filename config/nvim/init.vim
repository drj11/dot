filetype off
syntax off
set noloadplugins
set nohlsearch
mapclear
set mouse=
" inspired by https://stackoverflow.com/a/13731714/242457
let &colorcolumn=join(range(73,80),",")
highlight ColorColumn ctermbg=234
execute $EXINIT

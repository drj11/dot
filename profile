unalias -a
export EXINIT="set sw=2|set ts=8|set wm=8|set expandtab|set ai|set nu"
export VISUAL=$(which vi)

# -X Avoid screen clear when quitting man page on Linux
# -R Interpret ANSI colour codes (useful for git, which uses them)
export LESS="-X -R"

# Avoid writing .pyc files.
export PYTHONDONTWRITEBYTECODE=x

addpath () {
  case "$PATH" in
    *$1*) ;; # do nothing
    *) PATH=$1:$PATH ;;
  esac
}
    
# Python likes to add scripts here.
addpath $HOME/.local/bin

# PS1=$(printf '\033[42m%s@%s\033[K\033[m$ ' "$USER" "$(hostname)")
case "$TERM" in
  (linux)
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ ' ;;
  (*) # From original .bashrc
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ ' ;;
esac

case "$(uname -s)" in
  (Darwin)
    export PS1='; ' ;;
esac


export GOPATH=~/go
addpath /usr/local/go/bin
addpath $GOPATH/bin
# Apple Font Tools
addpath /Library/Apple/usr/bin
# Brew install python
addpath /usr/local/opt/python/libexec/bin
# My best guess at where Brew-install python installs binaries
# using pip:
addpath /usr/local/opt/python/Frameworks/Python.framework/Versions/Current/bin


# Avoid telemtry in dotnet
export DOTNET_CLI_TELEMETRY_OPTOUT=1

# Work round a bug in harfbuzz https://github.com/harfbuzz/harfbuzz/issues/4051
export HB_DRAW=0

# For sharc
# addpath ~/opt/go/bin
if [ -d ~/opt/go ]; then
  export GOROOT=~/opt/go
fi

# ruby
addpath ~/.rbenv/shims

# For conda
if [ -f ~/"miniconda3/etc/profile.d/conda.sh" ]; then
  . ~/"miniconda3/etc/profile.d/conda.sh"
fi
